FROM python:3.11-rc-bullseye

RUN apt-get update && apt-get install -y cron bc

COPY . /app

RUN pip3 install --no-cache-dir --upgrade pip && \
    pip3 install --no-cache-dir -r app/requirements.txt

RUN chmod 0644 /app/cronfile &&\
    crontab /app/cronfile

# Inspired from : https://www.cloudsavvyit.com/9033/how-to-use-cron-with-your-docker-containers/
ENTRYPOINT ["cron", "-f", "-L", "15"]
