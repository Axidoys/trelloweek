# Trelloweek

## Run

### as a container

sudo docker run -d --env-file /home/username/trelloWeek/env-file registry.gitlab.com/axidoys/trelloweek/image

### as a container (built by ourselves)

For example on arm64, can't be done through GitlabCI

sudo docker build -t trelloweek .

sudo docker run -d --env-file /home/username/trelloWeek/env-file trelloweek


### as a cronjob

pip3 install -r requirements.txt

Install the cronfile from this repo with correct path used