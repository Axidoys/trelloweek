import requests
import sys
import os
import datetime
from dotenv import load_dotenv

load_dotenv()

if not 'BOARD_ID' in os.environ:
    print("BOARD_ID is not defined in env")
    sys.exit(-2)
BOARD_ID = os.getenv('BOARD_ID')
if not 'BASE_URL' in os.environ:
    print("BASE_URL is not defined in env")
    sys.exit(-2)
BASE_URL = os.getenv('BASE_URL')
if not 'TRELLO_KEY' in os.environ:
    print("TRELLO_KEY is not defined in env")
    sys.exit(-2)
KEY= os.getenv('TRELLO_KEY')
if not 'TRELLO_TOKEN' in os.environ:
    print("TRELLO_TOKEN is not defined in env")
    sys.exit(-2)
TOKEN=os.getenv('TRELLO_TOKEN')

print("[" + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") +
      "] TrelloWeek has been called with argument " + sys.argv[1]
      )

# get lists
res = requests.get(BASE_URL+"boards"+"/"+BOARD_ID+"/"+"lists?key="+KEY+"&token="+TOKEN)
listsObj = res.json()


# TODO check return of request


# list cards of day
rstday = int(sys.argv[1])
res = requests.get(BASE_URL+"lists"+"/"+listsObj[rstday]["id"]+"/"+"cards?key="+KEY+"&token="+TOKEN)
print(f'#{listsObj[rstday]["name"]}')

# remove cards
payload = dict(key=KEY, token=TOKEN)
ids = [ c["id"] for c in res.json() ]
for id in ids :
    print(id)
    res = requests.request("DELETE", BASE_URL+"cards"+"/"+str(id), data=payload)


# TODO check return of request


# add cards
CARDS=["Matin", "Midi", "Soir"]
for cardName in CARDS :
    query = {
       'key': KEY,
       'token': TOKEN,
       'idList': listsObj[rstday]["id"],
       'name': cardName
    }
    response = requests.request(
       "POST",
       "https://api.trello.com/1/cards",
       params=query
    )


# TODO check return of request


# if change a week

#if arg => change specific day
#else

'''
payload = dict(key1='value1', key2='value2')
res = requests.post(url, data=payload)


print(res.text)
'''
